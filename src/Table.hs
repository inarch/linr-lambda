module Table where

import Data.List (intercalate, transpose)
import Util (for)

data Align = AlignLeft | AlignRight | AlignCenter

padLeft :: Int -> String -> String
padLeft w s | length s >= w = s
            | otherwise = replicate (w - length s) ' ' ++ s

padRight :: Int -> String -> String
padRight w s | length s >= w = s
             | otherwise = s ++ replicate (w - length s) ' '

center :: Int -> String -> String
center w s | length s >= w = s
           | otherwise     = let n = w - length s
                                 (x,y) = n `quotRem` 2
                                 l = replicate x       ' '
                                 r = replicate (x + y) ' '
                             in l ++ s ++ r

maxLength :: [String] -> Int
maxLength = maximum . map length

mapTable :: (a -> b) -> [[a]] -> [[b]]
mapTable f = map (map f)

f :: [[String]] -> String
f = intercalate "\n" . map unwords

showTable :: [Align] -> [[String]] -> String
showTable as rows = f out
  where cols  = transpose rows
        cols' = mapTable length cols
        widths = map maximum cols'
        afs = for as $ \x ->
                case x of
                  AlignLeft   -> padRight
                  AlignRight  -> padLeft
                  AlignCenter -> center
        afs' = afs ++ repeat padRight
        fs = map (\(f,w) -> f w) $ zip afs' widths
        out = transpose . map (uncurry map) $ zip fs cols
