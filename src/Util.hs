module Util ( ifJust
            , ifJustM
            , ifM
            , whenJust
            , whenJustM
            , writeBinaryFile
            , rewrapMaybe
            , readMaybe
            , for
            , checkPermissions

            -- re-export permission predicates
            , writable
            , readable
            , executable
            , searchable
            ) where

import System.Directory (doesFileExist, getPermissions, Permissions, readable, writable, executable, searchable)
import System.IO (IOMode(WriteMode), openFile, hSetBinaryMode, hPutStr, hClose)

for :: [a] -> (a -> b) -> [b]
for = flip map

writeBinaryFile :: FilePath -> String -> IO ()
writeBinaryFile p s = do
  h <- openFile p WriteMode
  hSetBinaryMode h True
  hPutStr h s
  hClose h

ifJust :: Maybe a -> (a -> b) -> b -> b
ifJust (Just x) f _ = f x
ifJust Nothing  _ g = g

ifJustM :: Monad m => m (Maybe a) -> (a -> m b) -> m b -> m b
ifJustM m f g = do
  v <- m
  case v of
    Just x -> f x
    _      -> g

ifM :: Monad m => m Bool -> m a -> m a -> m a
ifM mBool f g = mBool >>= \x -> if x then f else g

whenJust :: Monad m => Maybe a -> (a -> m ()) -> m ()
whenJust (Just x) f = f x
whenJust Nothing  _ = return ()

whenJustM :: Monad m => m (Maybe a) -> (a -> m ()) -> m ()
whenJustM v f = ifJustM v f (return ())

rewrapMaybe :: Maybe a -> Either () a
rewrapMaybe (Just v) = Right v
rewrapMaybe Nothing  = Left ()

readMaybe :: Read a => String -> Maybe a
readMaybe s = case reads s of
  [(x,"")] -> Just x
  _        -> Nothing

-- file properties

checkPermissions :: [Permissions -> Bool] -> FilePath -> IO Bool
checkPermissions ps path = do
  e <- doesFileExist path
  if e then do
    p <- getPermissions path
    return $ False `notElem` map ($ p) ps
   else
    return False
