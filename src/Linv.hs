module Linv where

import Control.Concurrent (forkIO)
import Control.Monad (void)
import Data.ByteString.Lazy.Char8 (pack)
import IN (IN)
import Linr (serialize)
import System.Directory (findExecutable)
import System.Exit (ExitCode(ExitSuccess, ExitFailure))
import System.Unix.Process (readProcessWithExitCode)

visualize :: IN -> IO ()
visualize n = do
  mLinvPath <- findExecutable "linv"
  case mLinvPath of
    Nothing -> putStrLn "Program `linv' not in $PATH."
    Just linvPath -> do
      (exitCode,out,err) <- readProcessWithExitCode linvPath [] id (pack . serialize $ n)
      case exitCode of
        ExitSuccess   -> return ()
        ExitFailure c -> putStrLn $ "ERROR: linv exited with code " ++ show c

forkVisualize :: IN -> IO ()
forkVisualize n = void $ forkIO (visualize n)
