module Show ( showExpr
            , showIN
            , showExpr'
            , inetToTable
            , unlines'
            , short
            ) where

import Data.List (intercalate)
import Data.Map (Map)
import Data.Map as M (fromList, toList, lookup)
import Data.Maybe (mapMaybe)
import Data.Set as S (Set, toList)
import Expr (Expr(EVar, EApp, EAbs, ELet), Var, bvar, fvar)
import IN.Internals (inVars, inGraph, IN, PortHandle, INCell, Port(..), INCell(..))
import Table (Align(..), showTable)

unlines' :: [String] -> String
unlines' = intercalate "\n"

showVar :: Var -> String
showVar = id

showSet :: Set String -> String
showSet s = "{" ++ intercalate "," (S.toList s) ++ "}"

showExpr :: Expr -> String
showExpr e = unlines' [exp, unwords ["BVar(e) =", b], unwords ["FVar(e) =", f]]
  where exp = "e = " ++ showExpr' e
        b = showSet $ bvar e
        f = showSet $ fvar e

short :: Expr -> String
short e =
  case e of
    EVar x    -> x
    EAbs x e1 -> showAbs [x] e1
    EApp _ _  -> showApp e
    _         -> "<<END"

showAbs :: [String] -> Expr -> String
showAbs acc (EAbs x e) = showAbs (acc ++ [x]) e
showAbs acc e          = "\\" ++ unwords acc ++ "." ++ short e

showApp (EApp (EApp a b) c) = short a ++ " " ++ short b ++ " " ++ short c
showApp (EApp v1@(EVar x1) v2@(EVar x2)) = short v1 ++ " " ++ short v2
showApp (EApp e1 e2) = "(" ++ short e1 ++ " " ++ short e2 ++ ")"

showExpr' :: Expr -> String
showExpr' = show
  where
    show (EApp e1 e2)   = "(" ++ show e1 ++ " " ++ show e2 ++ ")"
    show (EAbs v e)     = "(\\" ++ showVar v ++ "." ++ show e ++ ")"
    show (EVar v)       = showVar v
    show (ELet v e1 e2) = "(let " ++ showVar v ++ " = " ++ show e1 ++ " in " ++ show e2 ++ ")"

showIN :: IN -> String
showIN n =
  let alignment = [ AlignLeft  -- label
                  , AlignRight -- prin
                  , AlignLeft  -- var
                  , AlignRight -- aux1
                  , AlignLeft  -- var
                  , AlignRight -- aux2
                  , AlignLeft  -- var
                  , AlignRight -- aux3
                  , AlignLeft  -- var
                  ]
      cellTable = showTable alignment . ([["CELL","PRIN","","AUX1","","AUX2","","AUX3",""]] ++) . inetToTable
  in cellTable n

inetToTable :: IN -> [[String]]
inetToTable n =
  let m = reverseLookupTable n
  in map (showCell m . snd) . M.toList . inGraph $ n

reverseLookupTable :: IN -> Map Int Var
reverseLookupTable n =
  let mv = inVars n
      mc = inGraph n
      g  = getWire mc
  in M.fromList . mapMaybe g . M.toList $ mv

getWire :: Map Int INCell -> (Var,PortHandle) -> Maybe (Int,Var)
getWire mc x =
  let g (v,(i,p)) = M.lookup i mc >>= h p >>= \x -> return (x,v)
      h Prin (Cell _ p _)           = Just p
      h Aux1 (Cell _ _ (a1:as))     = Just a1
      h Aux2 (Cell _ _ (_:a2:as))   = Just a2
      h Aux3 (Cell _ _ (_:_:a3:as)) = Just a3
      h _ _                         = Nothing
  in g x

labelWire :: Map Int Var -> Int -> [String]
labelWire m w =
  case M.lookup w m of
    Just x  -> [show w, "[" ++ x ++ "]"]
    Nothing -> [show w, ""]

showCell :: Map Int Var -> INCell -> [String]
showCell m c =
  let f = labelWire m
  in case c of
    Cell l p as -> show l : f p ++ take 6 (concatMap f as ++ repeat "")
    Wire a b    -> "Wire" : f a ++ f b ++ replicate 4 ""
