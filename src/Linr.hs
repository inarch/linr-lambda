module Linr ( executeSocket
            , executeSocketTime
            , serialize
            , deserialize
            ) where

import Data.Bits (Bits(), shiftL, (.&.))
import Data.Char (chr, ord)
import Data.Either (rights)
import qualified Data.Map as M
import Data.Time.Clock (NominalDiffTime, getCurrentTime, diffUTCTime)
import IN.Internals
import Sock (sendRecv)
import Control.DeepSeq (deepseq)

executeSocketTime :: FilePath -> IN -> IO (Either String IN, NominalDiffTime)
executeSocketTime socketPath net =
  let serNet = serialize net in
    serNet `deepseq` return serNet >>= \n ->
      getCurrentTime >>= \t1 ->
        sendRecv socketPath n >>= \n' ->
          getCurrentTime >>= \t2 ->
            let m = case deserialize n' of
                      Left es  -> Left $ concat es
                      Right m' -> Right m'
            in return (m, t2 `diffUTCTime` t1)

executeSocket :: FilePath -> IN -> IO (Either String IN)
executeSocket socketPath net = fmap fst (executeSocketTime socketPath net)

-- serialization

toCode :: Integral a => INSymbol -> a
toCode One     =  1
toCode Ten     =  2
toCode Bottom  =  3
toCode Par     =  4
toCode Weak    =  5
toCode Contr   =  6
toCode Derel   =  7
toCode PromOut =  8
toCode PromIn  =  9
toCode Epsilon = 10
toCode Delta   = 11
toCode Omicron = 12
toCode LPlus   = 13
toCode RPlus   = 14
toCode With    = 15
toCode Switch  = 16
toCode Lambda  = 17
toCode Rho     = 18
toCode Rec     = 19
toCode Uncast  = 20
toCode Bcast   = 21

toSymbol :: Integral a => a -> INSymbol
toSymbol  1 = One
toSymbol  2 = Ten
toSymbol  3 = Bottom
toSymbol  4 = Par
toSymbol  5 = Weak
toSymbol  6 = Contr
toSymbol  7 = Derel
toSymbol  8 = PromOut
toSymbol  9 = PromIn
toSymbol 10 = Epsilon
toSymbol 11 = Delta
toSymbol 12 = Omicron
toSymbol 13 = LPlus
toSymbol 14 = RPlus
toSymbol 15 = With
toSymbol 16 = Switch
toSymbol 17 = Lambda
toSymbol 18 = Rho
toSymbol 19 = Rec
toSymbol 20 = Uncast
toSymbol 21 = Bcast

arity :: Integral a => INSymbol -> a
arity One     = 0
arity Ten     = 2
arity Par     = 2
arity Weak    = 0
arity Derel   = 1
arity Contr   = 2
arity PromOut = 2
arity PromIn  = 2
arity Uncast  = 0
arity Rec     = 3
arity Bcast   = 2

serializeCell' :: INCell -> [Int]
serializeCell' (Cell l p as) = sym:p:as where sym = toCode l
serializeCell' (Wire a b)    = [0,0,a,b]
serializeCell' (Free x)      = [0,1,x,x]

encodeNum :: (Integral a, Bits a) => a -> [a]
encodeNum n | n < 128   = [n]
            | otherwise = 128 + (n `mod` 128) : encodeNum (n `quot` 128)

byteEncode :: [Int] -> [Char]
byteEncode xs = map chr $ (length xs' + 1) : xs'
  where xs' = concatMap encodeNum xs

serializeCell :: INCell -> [Char]
serializeCell = byteEncode . serializeCell'

getWire :: PortHandle -> IN -> Int
getWire (h,port) n =
  let g = inGraph n
      (Just c) = M.lookup h g
      w (Cell _ p _)         Prin = p
      w (Cell _ _ (p:_))     Aux1 = p
      w (Cell _ _ (_:p:_))   Aux2 = p
      w (Cell _ _ (_:_:p:_)) Aux3 = p
  in w c port

-- TODO: add free wires
serialize :: IN -> [Char]
serialize n = a ++ b
  where g = inGraph n
        a = concatMap serializeCell . map snd . M.toList $ g
        b = concatMap (f . snd) . M.toList . inVars $ n
        f p = let w = getWire p n in byteEncode [0,1,w,w]

-- deserialization

decodeNum' :: (Bits a, Integral a) => [a] -> Int -> a -> (a,[a])
decodeNum' (x:xs) sh acc =
  let x'   = (x .&. 127) `shiftL` sh
      acc' = x' + acc
  in if x < 128 then
       (acc', xs)
      else
       decodeNum' xs (sh+7) acc'

decodeNum :: (Bits a, Integral a) => [a] -> (a,[a])
decodeNum ys = decodeNum' ys 0 0

toCell :: [Int] -> Either String INCell
toCell (0:0:x:[y]) = Right $ Wire x y
toCell (0:1:w1:w2) = Right $ Free w1
toCell (s:p:as)    =
  let s' = toSymbol s
  in if length as == arity s' then
       Right $ Cell (toSymbol s) p as
      else
       Left $ "arity mismatch (" ++ show s' ++ ")"

-- decode byte input into int sequence
decodeBody :: [Int] -> [Int]
decodeBody [] = []
decodeBody xs =
  let (y,zs) = decodeNum xs
  in if null zs then
       [y]
      else
       y : decodeBody zs

{-
 - The first byte(s) of a sequence is the encoding of the length of the
 - sequence. Then follow the encoded label, encoded principal port and
 - encoded auxilary ports.
 -
 - Note: toSeqs may return invalid sequences if input list is too short.
 -}
toSeqs :: [Int] -> [[Int]]
toSeqs [] = []
toSeqs xs =
  let (l,ys) = decodeNum xs
      (s,zs) = splitAt (l-1) ys
  in decodeBody s : toSeqs zs

deserialize :: [Char] -> Either [String] IN
deserialize = toNet . rights . map toCell . toSeqs . map ord
  where
    toNet cs = let (n,_) = empty [0..] [0,2..]
               in Right $ n { inGraph = M.fromList $ zip [0..] cs }
