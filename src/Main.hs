module Main where

import           Control.Applicative ((<$>))
import           Control.Exception (try)
import           Control.Monad (forM, forM_, void, when, unless)
import           Control.Monad.IO.Class (liftIO)
import           Control.Monad.State (StateT, get, modify, runStateT)
import qualified Data.Graph as G (graphFromEdges, topSort)
import           Data.List (isPrefixOf, intercalate)
import           Data.Map (Map)
import qualified Data.Map as M
import           Data.Maybe (isJust, fromJust)
import           Data.Set (Set)
import qualified Data.Set as S
import           Env (getEnvVar, processArgs, expandPath)
import qualified Exec as EX (ExecEnv(..), execEnvSock, Result, resultNet, resultReduct, resultExecTime, resultExpr, resultNet, run, runProgram)
import           Expr (Expr, Var, expLet, fvar)
import           IN (IN)
import qualified IN
import           Latex (exprToMath, exprToTree)
import qualified Linr as LR (serialize)
import qualified Linv as LV (forkVisualize)
import           Parser (defaultLang, extendedLang, ParserSettings, parseExpLang, parseBindExp)
import           Show
import           System.Environment (getArgs)
import           System.IO (IOMode(ReadMode), stdout, hFlush, openFile, hGetContents)
import           System.IO.Error (isEOFError, ioeGetErrorString)
import           Text.Printf (printf)
import           Util (ifM, ifJustM, whenJust, writeBinaryFile, whenJustM, checkPermissions, readable)
import           Version

data ShellCommand = SCQuit
                    deriving (Show, Eq)

data ShellState = ShellState { stLang       :: ParserSettings
                             , stPrompt     :: String
                             , stCmd        :: Maybe ShellCommand
                             , stLinrSocket :: Maybe FilePath
                             , stLastExpr   :: Maybe Expr
                             , stLastNet    :: Maybe IN
                             , stLastReduct :: Maybe IN
                             , stAutoReduce :: Bool
                             , stBindings   :: Map Var Expr
                             }

instance Show ShellState where
  show s = "ShellState {stLang = " ++ show (stLang s) ++
           ", stPrompt = \"" ++ stPrompt s ++ "\"" ++
           ", stCmd = " ++ show (stCmd s) ++
           ", stLinrSocket = " ++ show (stLinrSocket s) ++
           ", stLastExpr = " ++ show (stLastExpr s) ++
           ", stBindings = " ++ show (stBindings s) ++
           "}"

type CmdT = [String] -> StateT ShellState IO ()

defaultShellState :: ShellState
defaultShellState = ShellState { stLang       = extendedLang
                               , stPrompt     = "> "
                               , stCmd        = Nothing
                               , stLinrSocket = Nothing
                               , stLastExpr   = Nothing
                               , stLastNet    = Nothing
                               , stLastReduct = Nothing
                               , stAutoReduce = True
                               , stBindings   = M.empty
                               }

getDefaultShellState :: IO ShellState
getDefaultShellState = do
  let st = defaultShellState
  ifJustM (getEnvVar "LINR_SOCKET")
    (\p -> return $ st { stLinrSocket = Just p })
    (return st)

io :: IO a -> StateT b IO a
io = liftIO

ioPutStr :: String -> StateT a IO ()
ioPutStr = io . putStr

ioPutStrLn :: String -> StateT a IO ()
ioPutStrLn = io . putStrLn

setLang :: ParserSettings -> StateT ShellState IO ()
setLang newSettings = modify $ \s -> s { stLang = newSettings }

setLastExpr :: Expr -> StateT ShellState IO ()
setLastExpr e = do
  modify $ \s -> s { stLastExpr = Just e }
  setLastNet $ IN.encode e

getLastExpr :: StateT ShellState IO (Maybe Expr)
getLastExpr = stLastExpr <$> get

setLastNet :: IN -> StateT ShellState IO ()
setLastNet n = modify $ \s -> s { stLastNet = Just n }

getLastNet :: StateT ShellState IO (Maybe IN)
getLastNet = stLastNet <$> get

setLastReduct :: IN -> StateT ShellState IO ()
setLastReduct net = modify $ \s -> s { stLastReduct = Just net }

getLastReduct :: StateT ShellState IO (Maybe IN)
getLastReduct = stLastReduct <$> get

quit :: StateT ShellState IO ()
quit = modify $ \s -> s { stCmd = Just SCQuit }

-- shell

cmdUsage :: [(String,String,a)] -> StateT ShellState IO ()
cmdUsage cmds = g >> f
  where cmds' = map (\(x,y,_)->(x,y)) cmds
        overview = unlines [ programName ++ " " ++ programVersion
                           , replicate (length programName + length programVersion + 1) '-'
                           , ""
                           , "Input lambda-terms at the prompt."
                           , "A backslash (\\) is used instead of a lambda in terms."
                           , ""
                           , "Here are some example lambda-terms:"
                           , "  \\f x. f (f x)"
                           , "  (\\x.x) y"
                           , "  (\\f x. f (f x)) (\\f x. f (f (f x)))"
                           , ""
                           , "Variable names and identifiers can consist of alphabetic characters"
                           , "and suffixed 'prime' symbols. (E.g. x, y, PLUS, x')"
                           , ""
                           , "Commands must be prefixed with a colon (:some-command<ENTER>), "
                           , "unambigous prefixes of commands are also accepted (i.e. :q for :quit)."
                           ]
        f        = ioPutStr . unlines . map f' $ cmds'
        f' (x,y) = printf ("%" ++ show (maxLen + 1) ++ "s  %s") x y
        maxLen   = maximum . map (length . fst) $ cmds'
        g        = ioPutStrLn overview

withPathArgument :: (FilePath -> StateT a IO ()) -> [FilePath] -> StateT a IO ()
withPathArgument f args =
  if null args then
    ioPutStrLn "No filename argument given"
   else do
    e <- io . expandPath $ head args
    f e

withOneArgument :: (String -> StateT a IO ()) -> [String] -> StateT a IO ()
withOneArgument f args =
  if null args then
    ioPutStrLn "No argument given"
   else
    f (unwords args)

internalCommands :: [(String,String,CmdT)]
internalCommands =
  [ ( "help"
    , "Show help text"
    , const $ return ()
    )
  , ( "+let"
    , "Enable extended syntax"
    , const $ do { ioPutStrLn "Enabled extended syntax"
                 ; setLang extendedLang
                 }
    )
  , ( "-let"
    , "Disable extended syntax"
    , const $ do { ioPutStrLn "Disabled extended syntax"
                 ; setLang defaultLang
                 }
    )
  , ( "+autoreduce"
    , "Enable auto-reduce"
    , const $ do { modify $ \s -> s { stAutoReduce = True }
                 ; ioPutStrLn "Enabled auto-reduce"
                 }
    )
  , ( "-autoreduce"
    , "Disable auto-reduce"
    , const $ do { modify $ \s -> s { stAutoReduce = False }
                 ; ioPutStrLn "Disabled auto-reduce"
                 }
    )
  , ( "visualize"
    , "Visualize net encoding of last expression using `linv'"
    , cmdVisualize
    )
  , ( "rvisualize"
    , "Visualize last reduct using `linv'"
    , cmdRVisualize
    )
  , ( "write"
    , "Write net to given file"
    , cmdWrite
    )
  , ( "rwrite"
    , "Write reduced net to given file"
    , cmdRWrite
    )
  , ( "show"
    , "Show inet encoding of last expression"
    , const showNet
    )
  , ( "rshow"
    , "Show reduced inet, alias for :reduce"
    , const cmdReduce
    )
  , ( "quit"
    , "Quit linr-lambda"
    , const quit
    )
  , ( "latex-expr"
    , "Print last expression as LaTeX math markup"
    , const $ whenJustM getLastExpr (ioPutStrLn . exprToMath)
    )
  , ( "latex-tree"
    , "Print last expression as LaTeX (synttree) tree"
    , const $ whenJustM getLastExpr (ioPutStrLn . exprToTree)
    )
  , ( "reduce"
    , "Reduce net using `linr'"
    , const cmdReduce
    )
  , ( "run"
    , "Run program given as argument"
    , cmdRun
    )
  , ( "expr"
    , "Show expression"
    , const showLastExpr
    )
  , ( "let"
    , "Bind expression, e.g. :let TRUE = \\x y. x"
    , cmdLet
    )
  , ( "unlet"
    , "Forget given binding"
    , cmdUnlet
    )
  , ( "unlet-all"
    , "Forget all bindings"
    , cmdUnletAll
    )
  , ( "bindings"
    , "Show bindings"
    , cmdBindings
    )
  ]

internalCommand :: String -> StateT ShellState IO ()
internalCommand cmdline =
  let parts        = words cmdline
      cmd          = head parts
      fst' (x,_,_) = x
      p            = (cmd `isPrefixOf`) . fst'
      matches      = filter p internalCommands
      usage        = cmdUsage internalCommands
  in if not (null parts) then case matches of
    []             -> ioPutStrLn $ "Unknown command: `" ++ cmd ++ "'"
    [("help",_,_)] -> usage
    [(_,_,action)] -> action (tail parts)
    _ -> case lookup cmd . map (\(a,_,c) -> (a,c)) $ internalCommands of
           Just action -> action (tail parts)
           Nothing     -> ioPutStrLn $ "Ambiguous command: `" ++ cmd ++ "';\n" ++
                                       "Do you mean one of those?\n" ++
                                       "  " ++ intercalate ", " (map fst' matches)
   else
    usage

buildEnvLet :: [(Var,Expr)] -> Expr -> Either String Expr
buildEnvLet bs expr =
  let toNode (x,e) = (e, x, S.toList (fvar e))
      (graph,g,_)  = G.graphFromEdges . map toNode $ bs
      toBinding k  = let (a,b,_) = g k in (b,a)
      bs'          = reverse . map toBinding . G.topSort $ graph
  in if isEnvClosed bs then
       Right $ foldr expLet expr bs'
      else
       let open = S.toList . openBindings $ bs
       in Left $ msgEnvNotClosed open

processLine :: String -> StateT ShellState IO ()
processLine inLine = do
  lang <- fmap stLang get
  let parse = parseExpLang lang
  case inLine of
    [] -> return ()
    (x:xs)
      | x == ':'  -> internalCommand xs
      | otherwise -> case parse inLine of
                       Left  err  -> ioPutStrLn $ "Parser error: " ++ err
                       Right expr -> do
                         bindings <- (M.toList . stBindings) <$> get
                         expr' <- case buildEnvLet bindings expr of
                           Left e  -> do
                             ioPutStrLn $ "Error: " ++ e
                             return expr
                           Right e -> return e
                         setLastExpr expr'
                         autoReduce <- stAutoReduce <$> get
                         when autoReduce cmdReduce

mainLoop :: StateT ShellState IO ()
mainLoop = do
  prompt <- fmap stPrompt get
  ioPutStr prompt
  io . hFlush $ stdout
  eLine <- io (try getLine :: IO (Either IOError String))
  case eLine of
    Left err
      | isEOFError err -> io (putStrLn []) >> quit
      | otherwise      -> io (putStrLn (ioeGetErrorString err)) >> quit
    Right x -> processLine x
  cmd <- fmap stCmd get
  if isJust cmd then case fromJust cmd of
    SCQuit -> return ()
   else
    mainLoop

introText :: String
introText = unlines [ programName ++ " " ++ programVersion
                    , "Input lambda-terms at the prompt."
                    , "Type :help<ENTER> to view a short help text."
                    ]

interactiveMode :: IO ()
interactiveMode = do
  putStr introText
  initPath <- expandPath "~/.linr-lambda"
  initLines <- ifM (checkPermissions [readable] initPath)
    (do putStrLn $ "Reading `" ++ initPath ++ "'..."
        h <- openFile initPath ReadMode
        lines <$> hGetContents h)
    (return [])
  st <- getDefaultShellState
  void $ runStateT (mapM_ processLine initLines >> mainLoop) st

nonInteractiveMode :: ([(String,Maybe String)],[String]) -> IO ()
nonInteractiveMode (flags,args) =
  ifJustM (getEnvVar "LINR_SOCKET")
    (\sock -> do
       putStrLn "Running in non-interactive mode"
       let env = EX.ExecEnv { EX.execEnvSock = sock }
       results <- forM args (EX.runProgram env)
       forM_ (zip args results) (\(path,eResult) -> do
         putStrLn $ "Program `" ++ path ++ "'"
         case eResult of
           Left e  -> putStrLn e
           Right r -> displayResult r))
    (putStrLn "Please set environment variable LINR_SOCKET first.")

main :: IO ()
main = do
  args <- getArgs
  let table = []
  let pa@(flags,args') = processArgs args (M.fromList table)
  if null args' then
    interactiveMode
   else
    nonInteractiveMode pa

-- :reduce, :rshow, :run <path>

wrapExec :: (EX.ExecEnv -> IO (Either String EX.Result)) -> StateT ShellState IO ()
wrapExec f = do
  mSock <- stLinrSocket <$> get
  case mSock of
    Nothing -> ioPutStrLn $ "Please set environment variable LINR_SOCKET correctly and restart " ++ programName ++ "."
    Just sock -> do
      let env = EX.ExecEnv { EX.execEnvSock = sock }
      res <- liftIO (f env)
      case res of
        Left err -> do
          ioPutStrLn "Execution failed."
          ioPutStrLn err
        Right r -> do
          setLastReduct (EX.resultReduct r)
          whenJust (EX.resultExpr r) setLastExpr
          setLastNet $ EX.resultNet r
          io . displayResult $ r

displayResult :: EX.Result -> IO ()
displayResult r = do
  putStrLn . showIN . EX.resultReduct $ r
  putStrLn $ "Execution time: " ++ show (EX.resultExecTime r)

cmdReduce :: StateT ShellState IO ()
cmdReduce = ifJustM getLastNet f g
  where f n = wrapExec (`EX.run` n)
        g   = ioPutStrLn noLastReduct

cmdRun :: CmdT
cmdRun = withPathArgument $ \path -> wrapExec (`EX.runProgram` path)

-- :let, :unlet, :bindings

openBindings :: [(Var,Expr)] -> Set Var
openBindings bs =
  let free = foldl S.union S.empty . map (fvar . snd) $ bs
      bound = S.fromList . map fst $ bs
  in free `S.difference` bound

isEnvClosed :: [(Var,Expr)] -> Bool
isEnvClosed = S.null . openBindings

showEnvClosed :: StateT ShellState IO ()
showEnvClosed = do
  bs <- (M.toList . stBindings) <$> get
  unless (isEnvClosed bs)
    (do let open = S.toList . openBindings $ bs
        ioPutStrLn $ msgEnvNotClosed open)

cmdLet :: CmdT
cmdLet = withOneArgument $ \str ->
  case parseBindExp str of
    Left e -> do ioPutStrLn "Parse error:"
                 ioPutStrLn e
    Right b@(x,expr) -> do
      bs <- stBindings <$> get
      let bs' = M.insert x expr bs
      modify $ \s -> s { stBindings = bs' }

cmdUnlet :: CmdT
cmdUnlet = withOneArgument $ \k -> do
  bs <- stBindings <$> get
  modify $ \s -> s { stBindings = M.delete k bs }

cmdUnletAll :: CmdT
cmdUnletAll = const $ modify $ \s -> s { stBindings = M.empty }

cmdBindings :: CmdT
cmdBindings = const $ do
  bindings <- (M.toList . stBindings) <$> get
  forM_ bindings $ \(x,e) ->
    ioPutStrLn $ ":let " ++ x ++ " = " ++ showExpr' e
  showEnvClosed

-- :write, :rwrite

cmdWrite :: CmdT
cmdWrite =
  withPathArgument $ \path ->
    ifJustM getLastNet
      (\n -> do io . writeBinaryFile path . LR.serialize $ n
                ioPutStrLn $ "Output written to `" ++ path ++ "'")
      (ioPutStrLn noLastNet)

cmdRWrite :: CmdT
cmdRWrite =
  withPathArgument $ \path ->
    ifJustM getLastReduct
      (\r -> do io . writeBinaryFile path . LR.serialize $ r
                ioPutStrLn $ "Output written to `" ++ path ++ "'")
      (ioPutStrLn noLastReduct)

-- :visualize, :rvisualize

maybeVisualize :: StateT ShellState IO (Maybe IN) -> String -> StateT ShellState IO ()
maybeVisualize gen msg = ifJustM gen f g
  where f = io . LV.forkVisualize
        g = ioPutStrLn msg

cmdVisualize :: CmdT
cmdVisualize = const $ maybeVisualize getLastNet noLastNet

cmdRVisualize :: CmdT
cmdRVisualize = const $ maybeVisualize getLastReduct noLastReduct

-- :expr, :show

showLastExpr :: StateT ShellState IO ()
showLastExpr = ifJustM getLastExpr f g
  where f = ioPutStrLn . showExpr
        g = ioPutStrLn noLastExpr

showNet :: StateT ShellState IO ()
showNet = ifJustM getLastNet f g
  where f = ioPutStrLn . showIN
        g = ioPutStrLn noLastNet

-- messages

noLastExpr :: String
noLastExpr = "No last expression set."

noLastNet :: String
noLastNet = "No last expression/net set."

noLastReduct :: String
noLastReduct = "No reduct set."

msgEnvNotClosed :: [String] -> String
msgEnvNotClosed open =
  let open' = intercalate ", " open
  in "Environment not closed, bind " ++ open' ++ "."
