module Env ( processArgs
           , flagSet
           , getEnvVar
           , expandPath
           ) where

import Control.Applicative ((<$>))
import Control.Exception (try)
import Data.Map (Map)
import qualified Data.Map as M
import System.Directory (getHomeDirectory)
import System.Environment (getEnv)
import System.FilePath (pathSeparators, (</>))

getEnvVar :: String -> IO (Maybe String)
getEnvVar k = do
  eitherEnv <- try (getEnv k) :: IO (Either IOError String)
  return $ case eitherEnv of
    Left _  -> Nothing
    Right v -> Just v

processArgs :: [String] -> Map String Bool -> ([(String, Maybe String)],[String])
processArgs args table = go args table [] []
  -- go :: [String] -> Map String Bool -> [(String, Maybe String)] -> [String] -> ([(String,Maybe String)],[String])
  where
    go [] table acc1 acc2 = (reverse acc1, reverse acc2)
    go (a:as) table acc1 acc2 =
      case M.lookup a table of
        Just True -> let arg:as' = as
                     in go as' table ((a,Just arg):acc1) acc2
        Just False -> go as table ((a,Nothing):acc1) acc2
        Nothing -> go as table acc1 (a:acc2)

flagSet :: [(String,Maybe String)] -> String -> Bool
flagSet [] _                            = False
flagSet ((f,_):fs) needle | f == needle = True
                          | otherwise   = flagSet fs needle

expandPath :: FilePath -> IO FilePath
expandPath path =
  case path of
    '~':x:xs | x `elem` pathSeparators ->
      -- TODO: getHomeDirectory might fail, see documentation
      (</> xs) <$> getHomeDirectory
    _ -> return path
