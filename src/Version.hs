module Version where

import Data.Version

programName :: String
programName = "linr-lambda"

programVersion' :: Version
programVersion' = Version { versionBranch = [0,1,1]
                          , versionTags   = []
                          }

programVersion :: String
programVersion = showVersion programVersion'
