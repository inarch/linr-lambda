module Sock (sendRecv) where

import Data.ByteString.Char8 (ByteString, pack, unpack, append, empty, last, null)
import Network.Socket hiding (send, sendTo, recv, recvFrom)
import Network.Socket.ByteString
import Prelude hiding (last, null)

recvAll' :: Socket -> ByteString -> IO ByteString
recvAll' s buf = do
  dat <- recv s 4096
  let ret = return buf
  if null dat then ret
              else case last dat of
                     '\EOT' -> ret
                     _      -> recvAll' s $ buf `append` dat

recvAll :: Socket -> IO ByteString
recvAll s = recvAll' s empty

sendRecv :: FilePath -> String -> IO String
sendRecv socketPath dat = do
  sock <- socket AF_UNIX Stream defaultProtocol
  connect sock $ SockAddrUnix socketPath
  sendAll sock $ pack dat
  shutdown sock ShutdownSend
  out <- recvAll sock
  sClose sock
  return $ unpack out
