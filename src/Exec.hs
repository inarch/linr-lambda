module Exec ( ExecInput(..)
            , ToExecInput(..)
            , run
            , runProgram
            , runExpr
            , ExecEnv(..)
            , Result(..)
            ) where

import Data.List.Split (splitOn)
import Data.Time.Clock (NominalDiffTime)
import Expr (Expr)
import IN (IN, encode)
import Linr (executeSocketTime)
import Parser (parseExpLang, extendedLang)
import System.IO (IOMode(ReadMode), withFile, hGetContents)
import Util (checkPermissions, readable, writable)

data ExecInput = ExecExpr Expr
               | ExecFile FilePath
               | ExecIN IN
                 deriving (Show, Eq)

class ToExecInput a where
  toExecInput :: a -> ExecInput

instance ToExecInput ExecInput where
  toExecInput = id

instance ToExecInput IN where
  toExecInput = ExecIN

data ExecEnv = ExecEnv { execEnvSock :: FilePath }

data Result = Result { resultExpr     :: Maybe Expr
                     , resultFile     :: Maybe FilePath
                     , resultStr      :: Maybe String
                     , resultNet      :: IN
                     , resultReduct   :: IN
                     , resultExecTime :: NominalDiffTime
                     }

run :: ToExecInput i => ExecEnv -> i -> IO (Either String Result)
run env input = do
  let input' = toExecInput input
  envOk <- checkEnv env
  if envOk then
    case input' of
      ExecIN n   -> runIN env Nothing Nothing Nothing n
      ExecExpr e -> runIN env (Just e) Nothing Nothing (encode e)
      ExecFile p -> do
        e <- prepFile p
        case e of
          Left e      -> return $ Left e
          Right (e,s) -> runIN env (Just e) (Just p) (Just s) (encode e)
   else
    return $ Left badEnv

runExpr :: ExecEnv -> Expr -> IO (Either String Result)
runExpr env = run env . ExecExpr

runProgram :: ExecEnv -> FilePath -> IO (Either String Result)
runProgram env = run env . ExecFile

prepFile :: FilePath -> IO (Either String (Expr,String))
prepFile path = do
  ok <- checkPermissions [readable] path
  if ok then
    withFile path ReadMode $ \h -> do
      contents <- hGetContents h
      let contents' = unlines . map (head . splitOn "--") . lines $ contents
      case parseExpLang extendedLang contents' of
        Left e  -> return $ Left e
        Right e -> return $ Right (e,contents)
   else
    return $ Left (badFile path)

runIN :: ExecEnv
      -> Maybe Expr
      -> Maybe FilePath
      -> Maybe String
      -> IN
      -> IO (Either String Result)
runIN env mExpr mPath mStr n = do
  let sock = execEnvSock env
  (eitherN,t) <- executeSocketTime sock n
  return $ case eitherN of
    Left e   -> Left e
    Right n' ->
      let r  = mkResult n n' t
          r' = r { resultExpr = mExpr
                 , resultFile = mPath
                 , resultStr  = mStr
                 }
      in Right r'

-- internal functions

mkResult :: IN -> IN -> NominalDiffTime -> Result
mkResult net reduct time =
  Result { resultExpr     = Nothing
         , resultFile     = Nothing
         , resultStr      = Nothing
         , resultNet      = net
         , resultReduct   = reduct
         , resultExecTime = time
         }

checkEnv :: ExecEnv -> IO Bool
checkEnv env = do
  let sock = execEnvSock env
  checkPermissions [readable, writable] sock

-- error messages

badEnv :: String
badEnv = "Environment is not properly set up.\nHave you set LINR_SOCKET correctly?"

badFile :: String -> String
badFile f = "Cannot read from file `" ++ f ++ "'."
