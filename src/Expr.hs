module Expr ( Expr(..)
            , Var
            , expApp
            , expAbs
            , expVar
            , expLet
            , fvar
            , bvar
            ) where

import Data.Set (Set, (\\))
import qualified Data.Set as S

data Expr = EApp Expr Expr     -- e1 e2
          | EAbs Var Expr      -- \x.e
          | EVar Var           -- x
          | ELet Var Expr Expr -- let x = e1 in e2
            deriving (Show, Ord, Eq)

type Var = String

type Position = String

expApp :: Expr -> Expr -> Expr
expApp = EApp

expAbs :: String -> Expr -> Expr
expAbs = EAbs

expVar :: String -> Expr
expVar = EVar

expLet :: (String, Expr) -> Expr -> Expr
expLet (x,t1) = ELet x t1

bvar :: Expr -> Set Var
bvar = f S.empty
  where
    f acc (EAbs v e)     = f (v `S.insert` acc) e
    f acc (EApp e1 e2)   = f acc e1 `S.union` f acc e2
    f acc (EVar _)       = acc
    f acc (ELet x e1 e2) =
      let acc' = x `S.insert` acc
      in f acc' e1 `S.union` f acc' e2

fvar :: Expr -> Set Var
fvar (EVar x)       = S.singleton x
fvar (EApp e1 e2)   = fvar e1 `S.union` fvar e2
fvar (EAbs x e)     = fvar e \\ S.singleton x
fvar (ELet x e1 e2) =
  let f1 = fvar e1
      f2 = fvar e2
  in if x `S.member` f2 then
       (f1 `S.union` f2) \\ S.singleton x
      else
       f2
