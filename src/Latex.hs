module Latex ( exprToMath
             , exprToTree
             ) where

import Expr (Expr(EVar, EApp, EAbs, ELet))

exprToMath :: Expr -> String
exprToMath = f
  where
    f (EVar v)       = v
    f (EApp e1 e2)   = "(" ++ f e1 ++ "\\;" ++ f e2 ++ ")"
    f (EAbs v e)     = "(\\lambda{}" ++ v ++ "." ++ f e ++ ")"
    f (ELet v e1 e2) = "\\textrm{let } " ++ v ++ " = " ++ f e1 ++ "\\textrm{ in }" ++ f e2

exprToTree :: Expr -> String
exprToTree (EVar x) = "\\synttree[$" ++ x ++ "$]"
exprToTree e =
  let f (EVar x)       = "$" ++ x ++ "$"
      f (EApp e1 e2)   = "[$(\\;)$ [" ++ f e1 ++ "] [" ++ f e2 ++ "]]"
      f (EAbs x e)     = "[$\\lambda{}" ++ x ++ "$ [" ++ f e ++ "]]"
      f (ELet x e1 e2) = "[\\textsf{let} $" ++ x ++ "$ [" ++ f e1 ++ "] [" ++ f e2 ++ "]]"
  in "\\synttree" ++ f e
