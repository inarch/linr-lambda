module IN ( IN()
          , encode
          ) where

import qualified Data.Map as M
import qualified Data.Set as S
import           Expr (Expr(EApp, EAbs, EVar, ELet), fvar)
import           IN.Internals

-- encoding

encode' :: [Int] -> [Int] -> Expr -> IN
encode' hs ws (EVar x) =
  let (n,_)        = empty hs ws
      (n1,(d0,d1)) = derel n
      (n2,_)       = free x d0 n1
      (n3,_)       = ground d1 n2
  in n3
encode' hs ws (EAbs v e) =
  let -- common
      n               = encode' hs ws e
      (n1,(p0,p1,p2)) = par n
      (n2,_)          = attach p2 n1
      (n3,_)          = ground p0 n2
      -- v not free in e
      (m0,w) = weak n3
      (m1,_) = connect w p1 m0
      -- v free in e
      (k,_) = bind v p1 n3
  in if v `S.member` fvar e then k else m1
encode' hs ws (EApp l (EVar x)) =
  let nl               = encode' hs ws l
      (_,Just z)       = zero nl
      (nr1,_)          = empty (inHandles nl) (inWires nl)
      (nr2,(t0,t1,t2)) = ten nr1
      (nr3,_)          = free x t1 nr2
      n1               = merge nl nr3 (inHandles nr3) (inWires nr3)
      (n2,_)           = connect t0 z n1
      (n3,_)           = ground t2 n2
  in n3
encode' hs ws (EApp l r) =
  let nl              = encode' hs ws l
      nr              = encode' (inHandles nl) (inWires nl) r
      nr'             = promBox nr
      (_, Just lZero) = zero nl
      (_, Just rZero) = zero nr'
      n               = merge nl nr' (inHandles nr') (inWires nr')
      (n1,(t0,t1,t2)) = ten n
      (n2,_)          = connect t0 lZero n1
      (n3,_)          = connect t1 rZero n2
      (n4,_)          = ground t2 n3
  in n4
encode' hs ws (ELet v e1 e2) =
  let nl          = encode' hs ws e1
      nl'         = if v `M.member` inVars nl then recPromBox v nl else promBox nl
      (_,Just zl) = zero nl'
      nr          = encode' (inHandles nl') (inWires nl') e2
      (_,Just zr) = zero nr
      n1          = merge nl' nr (inHandles nr) (inWires nr)
      (n2,_)      = bind v zl n1
      (n3,_)      = ground zr n2
  in if v `S.member` fvar e2 then n3 else encode' hs ws e2

encode :: Expr -> IN
encode e =
  let n           = encode' [1..] [0,2..] e
      (_, Just z) = zero n
      (n1, _)     = free "0" z n
  in n1
