module IN.Internals where

import qualified Data.Map as M
import           Expr (Var)

data INCell = Cell INSymbol Int [Int]
            | Free Int
            | Wire Int Int
              deriving (Eq, Show)

data IN = IN { inGraph   :: M.Map Int INCell
             , inVars    :: M.Map Var PortHandle
             , inZero    :: Maybe PortHandle
             , inWires   :: [Int]
             , inHandles :: [Int]
             } deriving (Eq)

instance Show IN where
  -- do not show wires and handles as they are probably infinite
  show n = "inGraph = " ++ show (inGraph n) ++ ", inVars = " ++ show (inVars n) ++ ", inZero = " ++ show (inZero n)

data INSymbol = One     | Ten    | Bottom  | Par   | Weak    | Contr  | Derel
              | PromOut | PromIn | Epsilon | Delta | Omicron | LPlus  | RPlus
              | With    | Switch | Lambda  | Rho   | Rec     | Uncast | Bcast
                deriving (Eq, Show)

data Port = Prin | Aux1 | Aux2 | Aux3
            deriving (Eq, Ord, Show)

type PortHandle = (Int, Port)

open :: Int
open = -1

-- insert cell into inet
cellN :: Int -> INSymbol -> IN -> (IN, [PortHandle])
cellN n sym inet =
  if n > 3 then undefined
           else let c      = Cell sym open (replicate n open)
                    (h:hs) = inHandles inet
                    ps     = zip (repeat h) ports
                    ports  = case n of
                               0 -> [Prin]
                               1 -> [Prin, Aux1]
                               2 -> [Prin, Aux1, Aux2]
                               3 -> [Prin, Aux1, Aux2, Aux3]
                               _ -> error "arity: 0 <= arity <= 3"
                    inet'  = inet { inGraph   = M.insert h c (inGraph inet)
                                  , inHandles = hs
                                  }
                in (inet',ps)

-- insert free wire into inet
free :: Var -> PortHandle -> IN -> (IN,())
free x p n =
  let (w:ws) = inWires n
      n1     = adjust p w n
      n2     = n1 { inWires = ws
                  , inVars  = M.insert x p (inVars n)
                  }
  in (n2,())

-- mark port as "ground"/0
ground :: PortHandle -> IN -> (IN, ())
ground h n = (n { inZero = Just h }, ())

zero :: IN -> (IN, Maybe PortHandle)
zero n = (n, inZero n)

-- attach ground to port
attach :: PortHandle -> IN -> (IN, ())
attach h n =
  let (_, Just z) = zero n
      (n', _)     = connect h z n
  in (n' { inZero = Nothing }, ())

-- adjust wire id of port
adjust :: PortHandle -> Int -> IN -> IN
adjust (handle,port) w' inet =
  let f = case port of
            Prin -> \c -> case c of
                            Cell x w ws -> Cell x w' ws
                            Free w      -> Free w'
            Aux1 -> \c@(Cell x w1 (w:ws))        -> Cell x w1 (w':ws)
            Aux2 -> \c@(Cell x w1 (w2:w3:ws))    -> Cell x w1 (w2:w':ws)
            Aux3 -> \c@(Cell x w1 (w2:w3:w4:ws)) -> Cell x w1 (w2:w3:w':ws)
  in inet { inGraph = M.adjust f handle (inGraph inet) }

-- "bind" variable (remove variable and reconnect)
bind :: Var -> PortHandle -> IN -> (IN,())
bind x p2 n =
  let vs     = inVars n
      vs'    = M.delete x vs
      (Just p1) = M.lookup x vs
      (n1,_) = connect p1 p2 n
      n2     = n1 { inVars = vs' }
  in (n2,())

-- connect two ports
connect :: PortHandle -> PortHandle -> IN -> (IN, ())
connect ph1@(h1,p1) ph2@(h2,p2) n =
  let (w:ws) = inWires n
      n1 = adjust ph1 w n
      n2 = adjust ph2 w n1
      n3 = n2 { inWires = ws }
  in (n3, ())

empty :: [Int] -> [Int] -> (IN, ())
empty handles wires =
  let n = IN { inGraph   = M.empty
             , inVars    = M.empty
             , inZero    = Nothing
             , inHandles = handles
             , inWires   = wires
             }
  in (n,())

deleteVar :: Var -> IN -> IN
deleteVar x n = n { inVars = M.delete x $ inVars n }

linkVars :: [(Var,(PortHandle,PortHandle))] -> IN -> IN
linkVars [] n = n
linkVars (v@(x,(p1,p2)):vs) n =
  let n1     = deleteVar x n
      n2     = deleteVar x n1
      (n4,(c0,c1,c2)) = contr n2
      (n5,_) = connect p1 c2 n4
      (n6,_) = connect p2 c1 n5
      (n7,_) = free x c0 n6
  in linkVars vs n7

-- merge two nets, shared variables will be linked by contr cell
merge :: IN -> IN -> [Int] -> [Int] -> IN
merge n1 n2 hs ws =
  let n = IN { inGraph   = inGraph n1 `M.union` inGraph n2
             , inVars    = M.empty
             , inZero    = Nothing
             , inHandles = hs
             , inWires   = ws
             }
      v1 = inVars n1
      v2 = inVars n2
      vs = M.toList $ M.intersectionWith (\x y -> (x,y)) v1 v2
      r1 = linkVars vs n
      r2 = r1 { inVars = inVars r1 `M.union` (v1 `M.difference` v2) `M.union` (v2 `M.difference` v1) }
  in if null vs then n { inVars = v1 `M.union` v2 }
                else r2

-- some convenience functions

cell0 :: INSymbol -> IN -> (IN, PortHandle)
cell0 sym net = let (net', [h]) = cellN 0 sym net
            in (net', h)

cell1 :: INSymbol -> IN -> (IN, (PortHandle, PortHandle))
cell1 sym net = let (net', [h1,h2]) = cellN 1 sym net
                in (net', (h1, h2))

cell2 :: INSymbol -> IN -> (IN, (PortHandle, PortHandle, PortHandle))
cell2 sym net = let (net', [h1,h2,h3]) = cellN 2 sym net
                in (net', (h1, h2, h3))

cell3 :: INSymbol -> IN -> (IN, (PortHandle, PortHandle, PortHandle, PortHandle))
cell3 sym net = let (net', [h1,h2,h3,h4]) = cellN 3 sym net
                in (net', (h1, h2, h3, h4))

ten :: IN -> (IN, (PortHandle, PortHandle, PortHandle))
ten = cell2 Ten

par :: IN -> (IN, (PortHandle, PortHandle, PortHandle))
par = cell2 Par

weak :: IN -> (IN, PortHandle)
weak = cell0 Weak

derel :: IN -> (IN, (PortHandle, PortHandle))
derel = cell1 Derel

contr :: IN -> (IN, (PortHandle, PortHandle, PortHandle))
contr = cell2 Contr

promOut :: IN -> (IN, (PortHandle, PortHandle, PortHandle))
promOut = cell2 PromOut

promIn :: IN -> (IN, (PortHandle, PortHandle, PortHandle))
promIn = cell2 PromIn

uncast :: IN -> (IN, PortHandle)
uncast = cell0 Uncast

bcast :: IN -> (IN, (PortHandle, PortHandle, PortHandle))
bcast = cell2 Bcast

rec :: IN -> (IN, (PortHandle, PortHandle, PortHandle, PortHandle))
rec = cell3 Rec

-- insert prom-ins and reconnect variable wires
insertPromIns' :: [(Var, PortHandle)] -> IN -> [PortHandle] -> (IN, [PortHandle])
insertPromIns' ((x,p):xs) n acc =
  let (n1,(p0,p1,p2)) = promIn n
      n2              = deleteVar x n1
      (n3,_)          = free x p2 n2
      (n4,_)          = connect p p1 n3
  in insertPromIns' xs n4 (p0:acc)
insertPromIns' [] n acc = (n, reverse acc)

-- wrapper for insertPromIns', used for constructing application prom-box
insertPromIns :: IN -> (IN, [PortHandle])
insertPromIns n = insertPromIns' (M.toList . inVars $ n) n []

--insertRecPromIns :: Var -> IN -> (IN, [PortHandle])
insertRecPromIns v n =
  let vs      = M.toList . M.delete v . inVars $ n
      (n',ps) = insertPromIns' vs n []
  in (n',ps)

-- insert broadcast tree
insertBcastTree :: [PortHandle] -> PortHandle -> IN -> IN
insertBcastTree [p] c n =
  let (n1,_) = connect p c n
  in n1
insertBcastTree (p:ps) c n =
  let (n1,(b0,b1,b2)) = bcast n
      (n2,_)          = connect b0 c n1
      (n3,_)          = connect p b1 n2
  in insertBcastTree ps b2 n3
insertBcastTree [] c n =
  let (n1,b0) = uncast n
      (n2,_)  = connect b0 c n1
  in n2

promBox :: IN -> IN
promBox n =
  -- TODO: only if there are free variables?
  let (n1,(p0,p1,p2)) = promOut n
      (n2,ps)         = insertPromIns n1
      n3              = insertBcastTree ps p2 n2
      (n4,_)          = attach p1 n3
      (n5,_)          = ground p0 n4
  in n5

recPromBox :: Var -> IN -> IN
recPromBox v n =
  let (n1,(p0,p1,p2,p3)) = rec n
      (n2,ps)            = insertRecPromIns v n1
      n3                 = insertBcastTree ps p3 n2
      (n4,_)             = attach p1 n3
      (n5,_)             = bind v p2 n4
      (n6,_)             = ground p0 n5
  in n6
