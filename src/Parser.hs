{-# OPTIONS_GHC -fno-warn-unused-do-bind #-}

module Parser ( program
              , expr
              , app
              , abs
              , var
              , oneExpr
              , identifier
              , defaultLang
              , extendedLang
              , ParserSettings
              , parseExp
              , parseExpLang
              , parseBindExp
              ) where

import Control.Monad (void)
import Expr (Expr, expVar, expApp, expAbs, expLet)
import Prelude hiding (abs)
import Text.ParserCombinators.Parsec ( try
                                     , Parser
                                     , eof
                                     , sepBy1
                                     , (<|>)
                                     , letter
                                     , alphaNum
                                     , char
                                     , string
                                     , many
                                     , many1
                                     , (<?>)
                                     , parse
                                     )

data ParserSettings = ParserSettings { psAllowLet :: Bool
                                     , psReservedWords :: [String]
                                     }
                      deriving (Show, Eq, Ord)

defaultLang :: ParserSettings
defaultLang = ParserSettings { psAllowLet = False
                             , psReservedWords = []
                             }

extendedLang :: ParserSettings
extendedLang = defaultLang { psAllowLet = True
                           , psReservedWords = ["let", "in"]
                           }

-- * Tokens

separator :: Parser Char
separator = char ' ' <|> char '\t' <|> char '\n'

whiteSpace :: Parser String
whiteSpace = many separator <?> "whitespace"

_identifier :: ParserSettings -> Parser String
_identifier s = do
  c1 <- letter
  cs <- many alphaNum
  let cs' = c1:cs
  if cs' `elem` psReservedWords s then
    fail "reserved word"
   else do
    primes <- many $ char '\''
    whiteSpace
    return $ cs' ++ primes

-- | Parse identifier: an identifier can consist of one or more
--   alphanumerical characters followed by any number of prime
--   symbols (').
identifier :: ParserSettings -> Parser String
identifier s = try (_identifier s) <|> fail "maybe reserved word"

lambda :: Parser ()
lambda = void $ (char '\\' <?> "lambda") >> whiteSpace

dot :: Parser ()
dot = void $ char '.' >> whiteSpace

lparen :: Parser ()
lparen = void $ char '(' >> whiteSpace

rparen :: Parser ()
rparen = void $ char ')' >> whiteSpace

keyword :: String -> Parser ()
keyword s = void $ string s >> separator >> whiteSpace

-- * Language

-- | Parse program: a program can consist of one or more
--   expressions separated by semicolons (;).
program :: ParserSettings -> Parser [Expr]
program s = do
  es <- expr s `sepBy1` (char ';' >> whiteSpace)
  eof
  return es

oneExpr :: ParserSettings -> Parser Expr
oneExpr s = do
  e <- expr s
  eof
  return e

-- expression
expr :: ParserSettings -> Parser Expr
expr s = do
  whiteSpace
  if psAllowLet s then
    abs s <|> letExpr s <|> app s
   else
    abs s <|> app s

letBinding :: ParserSettings -> Parser (String,Expr)
letBinding s = do
  i <- identifier s
  char '=' >> whiteSpace
  e <- expr s
  return (i,e)

letExpr :: ParserSettings -> Parser Expr
letExpr s = do
  keyword "let"
  b <- letBinding s
  keyword "in"
  e <- expr s
  return $ expLet b e

-- application (includes single aexpr)
app :: ParserSettings -> Parser Expr
app s = do
  es <- many1 (aexpr s)
  return $ if length es > 1
    then foldl1 expApp es
    else head es

-- lambda abstraction
abs :: ParserSettings -> Parser Expr
abs s = do
  lambda
  vs <- many1 (identifier s)
  dot
  e <- expr s
  return $
    let lambdas = map expAbs vs
    in foldr ($) e lambdas

-- atomic expression
aexpr :: ParserSettings -> Parser Expr
aexpr s = var s <|> pexpr s

-- parenthesized expression
pexpr :: ParserSettings -> Parser Expr
pexpr s = do
  lparen
  e <- expr s
  rparen
  return e

-- variable
var :: ParserSettings -> Parser Expr
var s = do
  v <- identifier s
  whiteSpace
  return $ expVar v

-- interface

-- to be used for :let bindings
bindExp :: Parser (String,Expr)
bindExp = do
  let lang = extendedLang
  x <- identifier lang
  whiteSpace
  char '=' >> whiteSpace
  e <- expr lang
  eof
  return (x,e)

parseBindExp :: String -> Either String (String,Expr)
parseBindExp s =
  case parse bindExp "(user input)" s of
    Left e  -> Left $ show e
    Right x -> Right x

parseExpLang :: ParserSettings -> String -> Either String Expr
parseExpLang lang s =
  case parse (oneExpr lang) "string" s of
    Left pe -> Left (show pe)
    Right e -> Right e

parseExp :: String -> Either String Expr
parseExp = parseExpLang defaultLang
