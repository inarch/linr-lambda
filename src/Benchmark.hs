module Main where

import Control.Applicative ((<$>))
import Control.Monad (forM_)
import Expr (Expr(..))
import Show (showExpr)
import System.Environment (getArgs)
import System.Random (randomR, StdGen, split, newStdGen)
import Criterion.Main (defaultMain, bench, whnf)
import IN (encode)

genExpr :: (Double,Double) -> StdGen -> Int -> Expr
genExpr _           g 0 = let (x,_) = randomR ('a','z') g in EVar [x]
genExpr p@(pApp,pAbs) g h =
  let (n,g')  = randomR (0,1) g :: (Double,StdGen)
      (g1,g2) = split g'
      h'      = h - 1
      (x,g1') = (\(a,b) -> ([a],b)) . randomR ('a','z') $ g1
      f n | n <= pApp = EApp (genExpr p g1 h') (genExpr p g2 h')
          | n <= pAbs = EAbs x (genExpr p g2 h')
          | otherwise = ELet x (genExpr p g1' h') (genExpr p g2 h')
  in f n

genExprStd :: StdGen -> Int -> Expr
genExprStd = genExpr (0.2,0.2)

for :: [a] -> (a -> b) -> [b]
for = flip map

main :: IO ()
main = do
  g <- newStdGen
  let xs = [0..20]
  let es = for xs (genExpr (0,0) g)
  -- forM_ (zip [0..] es) (\(i,e) -> putStrLn $ show i ++ " " ++ showExpr e)
  let bs = for xs $ \n -> let expr = es !! n
                          in bench ("depth" ++ show n ++ "_") $ whnf encode expr
  defaultMain bs
