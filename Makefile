.PHONY: clean

b: build

p: profiling-binary

clean:
	find . -name \*~ -exec rm {} \;
	find src -name \*.hi -exec rm {} \;
	find src -name \*.o -exec rm {} \;

build:
	cabal configure && cabal build

profiling-binary:
	(cd src; ghc -prof -auto-all -rtsopts -o Main-prof Main.hs)
	# run with ./Main-prof +RTS -p -RTS and it will output Main-prof.prof
