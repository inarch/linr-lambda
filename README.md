linr-lambda
===========

Build instructions
------------------

Build the project using the following commands:

    $ cabal configure
    $ cabal build

At this point a binary should be placed below the `dist`
directory.

If you want to install the software on your computer type

    $ cabal install

instead of the above commands. If you want to install for the current
user only use this command:

    $ cabal install --user

Resolving dependency issues
---------------------------

In case any of the above commands inform you about missing dependencies
install them using `cabal install <dep>`.

Benchmarks and Profiling
------------------------

Type

    $ make profiling-binary

to build a `linr-lambda` binary with profiling enabled. Afterwards type the
following command to produce profiling output. The command

    $ src/Main-prof +RTS -p

will start `linr-lambda` with profiling enabled. When exiting the program a
file called `Main-prof.prof` holding profiling information will be created.

Consult the [GHC user's
guide](http://www.haskell.org/ghc/docs/7.2.1/html/users_guide/profiling.html)
for details on profiling Haskell programs.

To run benchmarks, you first have to install the package `criterion`.

    $ cabal install criterion

Afterwards you can run the benchmarks:

    $ cd src
    $ runghc Benchmarks.hs

Benchmarking output will be printed to the screen.
